FROM alpine
MAINTAINER Antoni Segura Puimedon "toni@kuryr.org"
WORKDIR /app

COPY . /app
RUN apk --no-cache --update add --virtual build-deps go \
  && go build \
  && apk del build-deps

CMD ["/app/app"]
