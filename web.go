package main

import (
	"fmt"
	"net"
	"net/http"
)

func address(device string) (dev_ip string) {
	ifaces, _ := net.Interfaces()
	dev_ip = ""
	for _, iface := range ifaces {
		if iface.Name == device {
			addrs, _ := iface.Addrs()
			for _, addr := range addrs {
				ip, _, _ := net.ParseCIDR(addr.String())
				if ip != nil {
					val := ip.To4()
						if val != nil {
							dev_ip = addr.String()
						}
				}
			}
		}
	}
	return dev_ip
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "%s) Hello Kuryr Webinar!\n", address("eth0"))
}

func main() {
        fmt.Println("My address is", address("eth0"))
	http.HandleFunc("/", handler)
	http.ListenAndServe(":5000", nil)
}
