import socket
import fcntl
import struct

from flask import Flask


app = Flask(__name__)

def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])

get_ip_address('eth0')
@app.route('/')
def hello_world():
    return '(%s) Hello Kuryr Webinar\n' % get_ip_address('eth0')

if __name__ == '__main__':
    print get_ip_address('eth0')
    app.run(debug=True,host='0.0.0.0')
